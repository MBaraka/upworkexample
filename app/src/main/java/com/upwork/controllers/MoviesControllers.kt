package com.upwork.controllers

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.upwork.api.ApiClient
import com.upwork.models.Movie
import io.reactivex.Observable
import io.realm.Realm
import org.json.JSONObject

/**
 * Created by M.Baraka on 20-Sep-17.
 */

object MoviesControllers {
    fun requestMovies(): Observable<List<Movie>> {
        return Observable.merge(
                loadFromCache(),
                requestMoviesFromServer()
        )
    }

    private fun requestMoviesFromServer(): Observable<List<Movie>> {
        val url = "https://api.themoviedb.org/3/discover/movie?api_key=${ApiClient.ApiKey}"
        return ApiClient.request(url)
                .map { parseResponse(it) }
                .doOnNext { saveToCache(it) }
    }

    private fun parseResponse(response: String): List<Movie> {
        val jsonObject = JSONObject(response)
        val jsonArray = jsonObject.getJSONArray("results")

        val typeToken = object : TypeToken<List<Movie>>() {}

        return Gson().fromJson(jsonArray.toString(), typeToken.type)
    }

    private fun saveToCache(movies: List<Movie>) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.delete(Movie::class.java)
        realm.copyToRealm(movies)
        realm.commitTransaction()
    }

    private fun loadFromCache(): Observable<List<Movie>> {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val movies = realm.where(Movie::class.java).findAll()
        realm.commitTransaction()
        return Observable.just(movies)
    }
}
