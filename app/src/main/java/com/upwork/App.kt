package com.upwork

import android.app.Application
import com.upwork.api.ApiClient
import io.realm.Realm

/**
 * Created by M.Baraka on 20-Sep-17.
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
    }
}