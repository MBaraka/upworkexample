package com.upwork.api

import com.upwork.BuildConfig
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import java.util.concurrent.Callable

object ApiClient {
    val ApiKey: String = "40a92e40f9634095420ebdcca1ae900d"
    private val client: OkHttpClient

    init {
        val builder = OkHttpClient.Builder().addInterceptor(ContentTypeInterceptor())
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(LoggingInterceptor())
        }
        client = builder.build()
    }

    //region Request
    fun request(url: String): Observable<String> =
            request(Request.Builder().url(url).build())


    fun request(request: Request): Observable<String> {
        return Observable.fromCallable {
            val response = client.newCall(request).execute()
            val isSuccessful = response.isSuccessful
            if (isSuccessful) {
                response.body().string()
            } else {
                throw RuntimeException("Server Error")
            }
        }.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread())
    }
    //endregion
}