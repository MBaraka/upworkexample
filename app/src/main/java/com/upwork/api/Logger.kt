package com.upwork.api

import android.util.Log
import com.upwork.BuildConfig

/**
 * Created by M.Baraka on 01-Aug-17.
 */

object Logger {
    fun i(tag: String, message: String) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, message)
        }
    }

    fun e(tag: String, message: String) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, message)
        }
    }

    fun e(throwable: Throwable) {
        if (BuildConfig.DEBUG) {
            Log.e("Logger", ">>>>>> Crash happened Check it!! <<<<<<")
            throwable.printStackTrace()
        }
    }

}
