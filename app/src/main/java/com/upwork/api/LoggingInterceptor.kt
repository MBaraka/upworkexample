package com.upwork.api


import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import java.io.IOException

class LoggingInterceptor : Interceptor {
    private val TAG = LoggingInterceptor::class.java.simpleName

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        Logger.i(TAG, request.toString())
        Logger.i(TAG, request.headers().toString())

        val response = chain.proceed(request)

        val body = response.body()
        val responseString = body.string()

        Logger.i(TAG, "received response ")
        Logger.i(TAG, responseString)

        val newBody = ResponseBody.create(body.contentType(), responseString)
        return response
                .newBuilder()
                .body(newBody)
                .build()
    }

}