package com.upwork.api

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

internal class ContentTypeInterceptor : Interceptor {
    @Throws(IOException::class) override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder().addHeader("Content-Type", "application/json").build()

        return chain.proceed(request)
    }
}