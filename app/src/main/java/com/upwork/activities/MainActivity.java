package com.upwork.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;
import com.upwork.R;
import com.upwork.adapters.MoviesAdapter;
import com.upwork.api.Logger;
import com.upwork.controllers.MoviesControllers;
import com.upwork.models.Movie;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class MainActivity extends RxAppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private MoviesAdapter moviesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        requestMovies();
    }


    private void requestMovies() {
        showLoading(true);
        MoviesControllers.INSTANCE.requestMovies().compose(this.<List<Movie>>bindToLifecycle())
                .subscribe(new Observer<List<Movie>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull List<Movie> movies) {
                        if (moviesAdapter == null) {
                            moviesAdapter = new MoviesAdapter(movies);
                            recyclerView.setAdapter(moviesAdapter);
                        } else {
                            moviesAdapter.setMovieList(movies);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Logger.INSTANCE.e(e);
                        showLoading(false);
                    }

                    @Override
                    public void onComplete() {
                        showLoading(false);
                    }
                });

    }


    private void showLoading(boolean value) {
        swipeRefreshLayout.setRefreshing(value);
    }

    @Override
    public void onRefresh() {
        requestMovies();
    }
}
