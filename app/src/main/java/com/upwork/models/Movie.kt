package com.upwork.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

/**
 * Created by M.Baraka on 20-Sep-17.
 */
open class Movie : RealmObject() {
    @SerializedName("vote_average")
    var voteAverage: Float = 0.0f

    var id: Int = -1

    @SerializedName("backdrop_path")
    var background: String = ""


    val backgroundFullPath: String
        get() = "http://image.tmdb.org/t/p/w320" + background

    var title: String = ""
}