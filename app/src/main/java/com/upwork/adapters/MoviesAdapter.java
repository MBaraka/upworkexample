package com.upwork.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.upwork.R;
import com.upwork.api.Logger;
import com.upwork.models.Movie;

import java.util.List;

/**
 * Created by M.Baraka on 20-Sep-17.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.Holder> {
    private List<Movie> movieList;

    public MoviesAdapter(@NonNull List<Movie> movieList) {
        this.movieList = movieList;
    }

    //region adapter functions
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.setMovie(movieList.get(position));
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public void setMovieList(List<Movie> movies) {
        this.movieList = movies;
        notifyDataSetChanged();
    }

    //endregion

    //region holder
    static class Holder extends RecyclerView.ViewHolder {
        private Movie movie;
        private TextView txt_voteAverage, txt_name;
        private ImageView img;
        private String TAG;

        Holder(View itemView) {
            super(itemView);
            TAG = getClass().getSimpleName();
            txt_name = itemView.findViewById(R.id.movieItem_txt_name);
            txt_voteAverage = itemView.findViewById(R.id.movieItem_txt_nativeName);
            img = itemView.findViewById(R.id.movieItem_img);
        }

        void setMovie(Movie movie) {
            this.movie = movie;
            txt_name.setText(movie.getTitle());
            txt_voteAverage.setText(itemView.getContext().getString(R.string.averageRate, movie.getVoteAverage()));

            Logger.INSTANCE.i(TAG, movie.getBackgroundFullPath());
            Glide.with(itemView.getContext()).load(movie.getBackgroundFullPath())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            Logger.INSTANCE.e("MoviesAdapter", "Image exception");
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean
                                isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    }).error(R.drawable.image_error).into(img);
        }
    }


    //endregion
}
